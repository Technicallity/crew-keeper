var app = angular.module("app", ["ngRoute"]);

app.config(function($routeProvider) {
    $routeProvider.when('/',
        {
            templateUrl: "partials/list.html",
            controller: "ListCtrl"
        }
    )
        .when('/add',
        {
            templateUrl: "partials/add.html",
            controller: "AppCtrl"
        }
    )
});

app.controller('ListCtrl', function($scope, $rootScope) {

    console.log('hello ListCtrl');

    if( $rootScope.crew == null) {
        $rootScope.crew = [
            {name: 'Kirk', description: 'Captain', service: 'Active'},
            {name: 'Spock', description: 'First Lieutenant', service: 'Active'},
            {name: 'Scotty', description: 'Lead Engineer', service: 'Active'},
            {name: 'Bones', description: 'Lead Medical Officer', service: 'Active'}
        ];
    }

});

app.controller('AppCtrl', function($scope, $location, $rootScope) {
    console.log('hello AppCtrl');

    $scope.addPerson = function() {
        //Let's developer know that the function is being called
        console.log('You have called the addPerson function!');

        //Assign's variables
        $scope.newPersonName = $scope.personName;
        $scope.newPersonDescription = $scope.personDescription;
        $scope.newPersonService = $scope.personService;

        $rootScope.crew.push({name:$scope.newPersonName, description:$scope.newPersonDescription, service:$scope.newPersonService});
        //$scope.crew.push({name: 'Jon', description: 'Awesome', service: 'Active'})
        //$scope.crew.push();

        console.log($scope.newPersonName);
        console.log($scope.newPersonDescription);
        console.log($scope.newPersonService);

        $location.path('/');

    }
});