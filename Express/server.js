//Initialize variables
var express = require("express"),
    http = require("http"),
    app = express();

//Express looks to find starting html file
app.use(express.static(__dirname + "/client"));

// Create our Express-powered HTTP server
http.createServer(app).listen(3000);
console.log("Server listening on port 3000");

// set up the root route
app.get("/", function (req, res) {
    res.send("This is the root route!");
});
